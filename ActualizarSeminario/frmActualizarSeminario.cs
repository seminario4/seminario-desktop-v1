﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace ActualizarSeminario
{
    public partial class frmActualizarSeminario : Form
    {
        public int id_facultad { get; set; }
        public frmActualizarSeminario()
        {
            InitializeComponent();
        }
        int control = 0;
        Negocios n = new Negocios();
        string codigo, nombre_sem, hora_ini, hora_fin, edificio, aula, ponente, fecha;
        int id_SeminarioProg, idEdifi, id_Docente, id_Aula,id_sem;
        private void frmActualizarSeminario_Load(object sender, EventArgs e)
        {
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;
            

            cbAula.DropDownStyle = ComboBoxStyle.DropDownList;
            cbNombrePonente.DropDownStyle = ComboBoxStyle.DropDownList;

            dateTimePicker1.CustomFormat = "HH:mm";
            cbEdificio.DropDownStyle = ComboBoxStyle.DropDownList;
            n.id_facultad = this.id_facultad;
            cbxFacultad.DataSource = n.listarFacultades();
            cbxFacultad.DisplayMember = "NOM_FACULTAD";
            cbxFacultad.ValueMember = "ID_FACULTAD";
            cbxFacultad.SelectedIndex = id_facultad;

            
            var tabla = n.listardoActualizable();
            if (tabla.Rows.Count >0)
            {
               
                for (int i = 0; i < tabla.Rows.Count; i++)
                {
                    codigo = tabla.Rows[i]["COD_SEMINARIO"].ToString();
                    nombre_sem = tabla.Rows[i]["NOM_SEMINARIO"].ToString();
                    hora_ini = tabla.Rows[i]["HORA_INICIO"].ToString();
                    hora_fin = tabla.Rows[i]["HORA_SALIDA"].ToString();
                    edificio = tabla.Rows[i][13].ToString();
                    aula = tabla.Rows[i]["NOM_AULA"].ToString();
                    fecha = tabla.Rows[i]["FECHA"].ToString();   
                    ponente = tabla.Rows[i]["NOMBRE_DOCENTE"].ToString();
                    id_SeminarioProg = int.Parse(tabla.Rows[i]["ID_SEM_PROG"].ToString());
                    idEdifi = int.Parse(tabla.Rows[i]["ID_EDIFICIO"].ToString());
                    id_Aula = int.Parse(tabla.Rows[i]["ID_AULA"].ToString());
                    id_Docente = int.Parse(tabla.Rows[i]["ID_DOCENTE"].ToString());
                    id_sem = int.Parse(tabla.Rows[i]["ID_SEMINARIO"].ToString());
                    dgvSeminarios.Rows.Add(codigo, nombre_sem, fecha, hora_ini, hora_fin, edificio, aula, ponente, "Seleccionar", id_SeminarioProg, idEdifi);
                }
                
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            int hora_i, hora_f, min_i, min_f;
            DateTime hora_inicio = Convert.ToDateTime(dateTimePicker1.Text.Trim());
            DateTime hora_final = Convert.ToDateTime(dateTimePicker2.Text.Trim());
            DateTime fecha = Convert.ToDateTime(dtpFecha.Text.Trim());
            int dia, mes, año;
            dia = fecha.Day;
            mes = fecha.Month;
            año = fecha.Year;
            hora_i = hora_inicio.Hour;
            hora_f = hora_final.Hour;
            min_f = hora_final.Minute;
            min_i = hora_inicio.Minute;

            if (txtnameSem.Text.Trim()==String.Empty)
            {
                MessageBox.Show("Seleccione un Seminario para Actualizar");
            }
            else if (hora_f - hora_i == 0)
            {
                MessageBox.Show("Verifique la hora de inicio y la hora de salida \n Como minimo programe una hora");
                dateTimePicker1.Focus();
            }
            else if (hora_f - hora_i < 0)
            {
                MessageBox.Show("Verifique la hora de inicio y la hora de salida \n Como minimo programe una hora");
                dateTimePicker1.Focus();
            }
            else
            {
                var id_docente = cbNombrePonente.SelectedValue.ToString();
                int id_doc = int.Parse(id_docente);
                var cup = cbAula.SelectedValue.ToString();
                int cupos = int.Parse(cup);
                n.nombre_aula = cbAula.Text.Trim();
                int id_aula = n.obtener_idAula();

                int minf = min_f - 1;
                int mini = min_i - 1;
                int horaf = hora_f;
                int horai = hora_i;
                if (mini == -1)
                {
                    horai = hora_i - 1;
                    mini = 00;
                }
                if (minf == -1)
                {
                    horaf = hora_f - 1;
                    minf = 00;
                }
                n.hora_f = horaf + ":" + minf;
                n.hora_i = horai + ":" + mini;
                n.id_aula = id_aula;

                n.id_docente = id_doc;
                n.id_SeminarioPROG = id_Semprog;
                n.fecha = año + "-" + mes + "-" + dia;
                var comprobar = n.vericar_seminario2();
                int tiempo = int.Parse(comprobar.Rows[0]["TIEMPO"].ToString());
                int presonal = int.Parse(comprobar.Rows[0]["PERSONA"].ToString());
                if (tiempo == 1)
                {
                    MessageBox.Show("Este Horario no esta disponible");
                    cbAula.Focus();
                }
                else if (presonal == 1)
                {
                    MessageBox.Show("Este Docente no esta disponible en este horario");
                    cbNombrePonente.Focus();
                }
                else
                {
                    n.id_facultad = this.id_facultad;
                    n.fecha = año + "-" + mes + "-" + dia;
                    n.hora_f = hora_f + ":" + min_f;
                    n.hora_i = hora_i + ":" + min_i;
                    n.id_aula = id_aula;
                    n.cupos = cupos;
                    n.nuevo_cupo = cupos;
                    n.id_docente = id_doc;
                    int cut = n.comprobar_cupos();
                    bool res=false;
                    if (cut ==1)
                    {
                        MessageBox.Show("La cantidad de alumnos que inscribieron este seminario es mayor a la capacidad de esta aula");
                        cbAula.Focus();
                    }
                    else
                    {
                         res= n.ActualizarSeminario();
                    }

                    if (res)
                    {
                        MessageBox.Show("Seminario Actualizado");
                        dateTimePicker1.Text = "00:00";
                        dateTimePicker2.Text = "00:00";
                        cbEdificio.SelectedIndex = -1;
                        cbNombrePonente.SelectedIndex = -1;
                        cbAula.SelectedIndex = -1;
                        dgvSeminarios.Rows.Clear();
                        var tabla = n.listardoActualizable();
                        if (tabla.Rows.Count > 0)
                        {
                            string fecha2;
                            int id_se;
                            for (int i = 0; i < tabla.Rows.Count; i++)
                            {
                                codigo = tabla.Rows[i]["COD_SEMINARIO"].ToString();
                                nombre_sem = tabla.Rows[i]["NOM_SEMINARIO"].ToString();
                                hora_ini = tabla.Rows[i]["HORA_INICIO"].ToString();
                                hora_fin = tabla.Rows[i]["HORA_SALIDA"].ToString();
                                edificio = tabla.Rows[i][13].ToString();
                                aula = tabla.Rows[i]["NOM_AULA"].ToString();
                                fecha2 = tabla.Rows[i]["FECHA"].ToString();
                                ponente = tabla.Rows[i]["NOMBRE_DOCENTE"].ToString();
                                id_SeminarioProg = int.Parse(tabla.Rows[i]["ID_SEM_PROG"].ToString());
                                idEdifi = int.Parse(tabla.Rows[i]["ID_EDIFICIO"].ToString());
                                id_Aula = int.Parse(tabla.Rows[i]["ID_AULA"].ToString());
                                id_Docente = int.Parse(tabla.Rows[i]["ID_DOCENTE"].ToString());
                                id_se = int.Parse(tabla.Rows[i]["ID_SEMINARIO"].ToString());
                                dgvSeminarios.Rows.Add(codigo, nombre_sem, fecha2, hora_ini, hora_fin, edificio, aula, ponente, "Seleccionar", id_SeminarioProg, idEdifi);
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Algo Salio mal");
                    }
                }
            }
        }

        

        

        private void cbEdificio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var idEdif = cbEdificio?.SelectedValue?.ToString();
                if (idEdif != "System.Data.DataRowView" && control == 1)
                {
                    int id = int.Parse(idEdif);
                    n.id_edificio = id;
                    cbAula.DataSource = n.listarAulas();
                    cbAula.DisplayMember = "NOM_AULA";
                    cbAula.ValueMember = "CAPACIDAD";
                    cbAula.SelectedText = nomAu;
                }
            }
            catch (Exception)
            {   }
            
            control = 1;
        }

        private void cbAula_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var capa = cbAula?.SelectedValue?.ToString();
                if (capa != "System.Data.DataRowView")
                {
                    int cap = int.Parse(capa);
                    txtCapacidad.Text = cap.ToString(); ;
                }
            }
            catch (Exception)
            {
            }

        }
        string nomAu, nombreEdi;
        int id_Semprog;
        private void dgvSeminarios_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            
            if (dgvSeminarios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Seleccionar"))
            {
                control = 0;
                int id_Sem = int.Parse(dgvSeminarios.Rows[e.RowIndex].Cells[9].Value.ToString());
                int id_edif = int.Parse(dgvSeminarios.Rows[e.RowIndex].Cells[10].Value.ToString());
                 nomAu=dgvSeminarios.Rows[e.RowIndex].Cells[6].Value.ToString();
                 nombreEdi = dgvSeminarios.Rows[e.RowIndex].Cells[5].Value.ToString();
                 DateTime fecha = Convert.ToDateTime(dgvSeminarios.Rows[e.RowIndex].Cells[2].Value.ToString());
                 string horai = dgvSeminarios.Rows[e.RowIndex].Cells[3].Value.ToString();
                 string horaf = dgvSeminarios.Rows[e.RowIndex].Cells[4].Value.ToString();
                 string semin_name =dgvSeminarios.Rows[e.RowIndex].Cells[1].Value.ToString();
                 string semin_cod = dgvSeminarios.Rows[e.RowIndex].Cells[0].Value.ToString();
                 id_Semprog = int.Parse(dgvSeminarios.Rows[e.RowIndex].Cells[9].Value.ToString());


                 txtnameSem.Text = semin_name + " " + semin_cod;
                 dateTimePicker1.Text = horai;
                 dateTimePicker2.Text = horaf;
                 n.id_SeminarioPROG = id_Sem;
                 dtpFecha.Text = fecha.ToString();
                DateTime localDate = DateTime.Now;
                this.dtpFecha.MinDate = localDate;
                


                cbxFacultad.Focus();
                cbxFacultad.SelectedIndex = id_facultad;

                cbEdificio.DataSource = n.listarEdif();
                cbEdificio.DisplayMember = "NOM_EDIF";
                cbEdificio.ValueMember = "ID_EDIFICIO";
                cbEdificio.SelectedIndex = id_edif-1;
                n.id_facultad = this.id_facultad;

                cbNombrePonente.DataSource = n.listarDOCENTE();
                cbNombrePonente.DisplayMember = "NOMBRE";
                cbNombrePonente.ValueMember = "ID_DOCENTE";



                var idEdif = cbEdificio.SelectedValue.ToString();

                    int id = int.Parse(idEdif);
                    n.id_edificio = id;
                    cbAula.DataSource = n.listarAulas();
                    cbAula.DisplayMember = "NOM_AULA";
                    cbAula.ValueMember = "CAPACIDAD";
                    cbAula.Text = nomAu;
                             

                
                    var capa = cbAula.SelectedValue.ToString();
                    if (capa != "System.Data.DataRowView")
                    {
                        int cap = int.Parse(capa);
                        txtCapacidad.Text = cap.ToString();
                        n.cupo_ante = cap;
                    }
                
            }

        }

        private void dgvSeminarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
