﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace AgregarEstudiante
{
    public partial class frmAgregarEstudiante : Form
    {
        public frmAgregarEstudiante()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
             DateTime localDate = DateTime.Now;
            cbxCarrera.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxFacultad.DataSource = n.listarFacultades();
            cbxFacultad.DisplayMember = "NOM_FACULTAD";
            cbxFacultad.ValueMember = "ID_FACULTAD";
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxFacultad.SelectedIndex = -1;
            cbxFacultad.Focus();
            
            ToolTip tt = new ToolTip();
            tt.SetToolTip(btnGuardar, "Guardar");
            tt.SetToolTip(btnCancelar, "Regresar Al Menú");
            dateTimePicker1.CustomFormat = "YY-mm-dd";
            int año = localDate.Year;
            año = año - 15;
            int mes = localDate.Month;
            int dia = localDate.Day;
            
             DateTime fechamax = Convert.ToDateTime(dia + "-" + mes + "-" + año);
            dateTimePicker1.MaxDate = fechamax;
            dateTimePicker1.CustomFormat = "YY-mm-dd";

        }
        string carrera;
        private void txtCarnet_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(mktxtCarnet, String.Empty);
        }

        
        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtEmail, String.Empty);
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtNombre, String.Empty);
        }

        private void txtApellido_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtApellido, String.Empty);
        }

        private void txtFacultad_SelectedItemChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbxFacultad, String.Empty);
        }

        private void cbxCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbxFacultad, String.Empty);
            var cod = cbxCarrera.SelectedValue.ToString();
            carrera = (cod);
            this.mktxtCarnet.Mask = carrera + "-0000-0000";
        }

        private void cbxFacultad_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbxFacultad, String.Empty);
            try
            {

                var id = cbxFacultad?.SelectedValue?.ToString();
                int idD = int.Parse(id);
                cbxCarrera.DataSource = n.listarcarreras(idD);
                cbxCarrera.DisplayMember = "NOM_CARRERA";
                cbxCarrera.ValueMember = "CODIGO";
                cbxCarrera.DropDownStyle = ComboBoxStyle.DropDownList;
                cbxCarrera.SelectedIndex = 0;
                var cod = cbxCarrera.SelectedValue.ToString();
                carrera = (cod);
                this.mktxtCarnet.Mask = carrera + "-0000-0000";
            }
            catch (Exception)
            {


            }


        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            if (cbxFacultad.Text.Trim() == String.Empty)
            {
                errorProvider1.SetError(cbxFacultad, "Campo Requerido");
            }
            else if (cbxCarrera.Text.Trim() == String.Empty)
            {
                errorProvider1.SetError(cbxCarrera, "Campo Requerido");
            }
            else if (mktxtCarnet.Text.Trim().Length<12)
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(mktxtCarnet, "Falta Completar");
                mktxtCarnet.Focus();
            }
            else if (mktxtCarnet.Text.Trim().Contains(" ")==true)
            {
                errorProvider1.SetError(mktxtCarnet,"No se permiten espacios en blanco");
            }            
            else if (String.IsNullOrEmpty(txtEmail.Text.Trim()))
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(txtEmail, "valor Requerido");
                txtEmail.Focus();
            }
            else if (String.IsNullOrEmpty(txtNombre.Text.Trim()))
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(txtNombre, "valor Requerido");
                txtNombre.Focus();
            }
            else if (String.IsNullOrEmpty(txtApellido.Text.Trim()))
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(txtApellido, "valor Requerido");
                txtApellido.Focus();
            }
            else if (String.IsNullOrEmpty(cbxFacultad.Text.Trim()))
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(cbxFacultad, "valor Requerido");
                cbxFacultad.Focus();
            }
            else if (String.IsNullOrEmpty(cbxCarrera.Text.Trim()))
            //IsNullOrEmpty=retorna falso o verdadero
            {
                errorProvider1.SetError(cbxCarrera, "valor Requerido");
                cbxCarrera.Focus();
            }
            else
            {
                string usuario =mktxtCarnet.Text.Replace("-","");
                n.nombre_usuario = usuario;
                DateTime fecha_na = Convert.ToDateTime(dateTimePicker1.Text);
                int mes = fecha_na.Month;
                int año = fecha_na.Year;
                int dia = fecha_na.Day;
                string pass = "";
                if (mes <10)
                {
                    pass=dia+"0"+mes+año;
                }
                else
                {
                    pass = (dia +""+ mes+""+ año).ToString(); ;
                }
                
                
                n.pwd = pass;
                int re =n.verificarUSuario();
                if (re!=0)
                {
                    MessageBox.Show("este carnet ya existe");
                    mktxtCarnet.Focus();
                    mktxtCarnet.Text = String.Empty;
                }
                else
                {

                    n.nombre_usuario = usuario;
                    n.pwd = pass;
                    var idFac = cbxFacultad.SelectedValue.ToString();
                    int idFacultad =int.Parse(idFac);
                    n.id_facultad = idFacultad;
                    n.nombres = txtNombre.Text.Trim().ToUpper();
                    n.apellidos = txtApellido.Text.Trim().ToUpper();
                    n.correo = txtEmail.Text.Trim();
                    n.nombre_carrera = cbxCarrera.Text.Trim();                    
                    n.carnet = mktxtCarnet.Text.Trim();
                    bool res =n.agregarAlumno();
                    if (res)
                    {
                        MessageBox.Show("Guardado Existosamente \n Nombre de Usuario : "+usuario+"\n Contraseña :"+pass);
                        
                        txtApellido.Text = String.Empty;
                        txtNombre.Text = String.Empty;
                        mktxtCarnet.Text = String.Empty;
                        txtEmail.Text = String.Empty;
                        cbxFacultad.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("Algo Falló");
                    }
                }
            }

        }

        private void mktxtCarnet_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(mktxtCarnet,String.Empty);
            string correo=mktxtCarnet.Text.Replace("-","");
            txtEmail.Text = correo + "@mail.utec.edu.sv";
        }
    }
}
