﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProgramarSeminario;
using ActualizarSeminario;
using ImprimirListado;
using listado_Sem;
namespace Botones
{
    public partial class Docente : Form
    {
        public int id_facultad { get; set; }
        public Docente()
        {
            InitializeComponent();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void btnProgramarSeminario_Click(object sender, EventArgs e)
        {

            DialogResult res1;
            frmProgramarSeminario fps = new frmProgramarSeminario();
            fps.id_facultad = this.id_facultad;
            this.Visible = false;
            res1 = fps.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                fps.Dispose();

            }
        }

        private void btnActualizarSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmActualizarSeminario ase = new frmActualizarSeminario();
            ase.id_facultad = this.id_facultad;
            this.Visible = false;
            res1 = ase.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                ase.Dispose();

            }
        }

        private void Docente_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmlist lis = new frmlist();
            lis.id_facultad = this.id_facultad;
            this.Visible = false;
            res1 = lis.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                lis.Dispose();

            }
        }
    }
}
