﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SeminariosInscritos;
using ConsultarSeminarios;
using InscribirSeminario;
namespace Botones
{
    public partial class Estudiante : Form
    {
        public Estudiante()
        {
            InitializeComponent();
        }
        public string USUARIO { get; set; }
        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void btnConsultarSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmConsultarSeminarios fcs = new frmConsultarSeminarios();
            this.Visible = false;
            res1 = fcs.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                fcs.Dispose();
            }
        }

        private void btnSeminariosInscritos_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmSeminariosInscritos vs = new frmSeminariosInscritos();
            vs.usuario = this.USUARIO;
            this.Visible = false;
            res1 = vs.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                vs.Dispose();
            }
        }

        private void btnInscribirSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmInscribirSeminario vs = new frmInscribirSeminario();
            vs.USUAR = this.USUARIO;
            this.Visible = false;
            res1 = vs.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                vs.Dispose();
            }
        }

        private void Estudiante_Load(object sender, EventArgs e)
        {

        }
    }
}
