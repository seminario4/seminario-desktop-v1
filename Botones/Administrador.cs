﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgregarEstudiante;
using AgregarDocente;
using AgregarSeminario;
using AgregarAdministrador;
using ProgramarSeminario;
using listado_Sem;
using ActualizarSeminario;

namespace Botones
{
    public partial class Administrador : Form
    {
        public Administrador()
        {
            InitializeComponent();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void btnAgregarEstudiante_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmAgregarEstudiante ra = new frmAgregarEstudiante();
            this.Visible = false;
            res1 = ra.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                ra.Dispose();

            }
        }

        private void btnAgregarDocente_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmAgregarDocente rd = new frmAgregarDocente();
            this.Visible = false;
            res1 = rd.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                rd.Dispose();

            }
        }

        private void btnAgregarSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmAgregarSeminario asem = new frmAgregarSeminario();
            this.Visible = false;
            res1 = asem.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                asem.Dispose();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmAgregarAdmin rad = new frmAgregarAdmin();
            this.Visible = false;
            res1 = rad.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                rad.Dispose();

            }
        }

        private void btnProgramarSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmProgramarSeminario fps = new frmProgramarSeminario();
            fps.id_facultad = -1;
            this.Visible = false;
            res1 = fps.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                fps.Dispose();

            }
        }

        private void btnListado_Seminarios_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmlist lis = new frmlist();
            lis.id_facultad = -1;
            this.Visible = false;
            res1 = lis.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                lis.Dispose();

            }
        }

        private void btnActualizarSeminario_Click(object sender, EventArgs e)
        {
            DialogResult res1;
            frmActualizarSeminario ase = new frmActualizarSeminario();
            ase.id_facultad = -1;
            this.Visible = false;
            res1 = ase.ShowDialog();
            if (res1 == DialogResult.Cancel)
            {
                this.Visible = true;
                ase.Dispose();

            }
        }
    }
}
