﻿namespace Botones
{
    partial class Estudiante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Estudiante));
            this.btnInscribirSeminario = new System.Windows.Forms.Button();
            this.btnSeminariosInscritos = new System.Windows.Forms.Button();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.btnConsultarSeminario = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnInscribirSeminario
            // 
            this.btnInscribirSeminario.Image = global::Botones.Properties.Resources.baseline_note_add_black_48dp;
            this.btnInscribirSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInscribirSeminario.Location = new System.Drawing.Point(322, 175);
            this.btnInscribirSeminario.Name = "btnInscribirSeminario";
            this.btnInscribirSeminario.Size = new System.Drawing.Size(404, 54);
            this.btnInscribirSeminario.TabIndex = 7;
            this.btnInscribirSeminario.Text = "Inscribir Seminario";
            this.btnInscribirSeminario.UseVisualStyleBackColor = true;
            this.btnInscribirSeminario.Click += new System.EventHandler(this.btnInscribirSeminario_Click);
            // 
            // btnSeminariosInscritos
            // 
            this.btnSeminariosInscritos.Image = global::Botones.Properties.Resources.baseline_list_alt_black_48dp;
            this.btnSeminariosInscritos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSeminariosInscritos.Location = new System.Drawing.Point(322, 271);
            this.btnSeminariosInscritos.Name = "btnSeminariosInscritos";
            this.btnSeminariosInscritos.Size = new System.Drawing.Size(404, 54);
            this.btnSeminariosInscritos.TabIndex = 6;
            this.btnSeminariosInscritos.Text = "Seminarios Inscritos";
            this.btnSeminariosInscritos.UseVisualStyleBackColor = true;
            this.btnSeminariosInscritos.Click += new System.EventHandler(this.btnSeminariosInscritos_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrarSesion.Image = global::Botones.Properties.Resources.keyboard_return;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(371, 459);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(305, 54);
            this.btnCerrarSesion.TabIndex = 9;
            this.btnCerrarSesion.Text = "Cerrar Sesion";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // btnConsultarSeminario
            // 
            this.btnConsultarSeminario.Image = global::Botones.Properties.Resources.baseline_manage_search_black_48dp1;
            this.btnConsultarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultarSeminario.Location = new System.Drawing.Point(322, 367);
            this.btnConsultarSeminario.Name = "btnConsultarSeminario";
            this.btnConsultarSeminario.Size = new System.Drawing.Size(404, 54);
            this.btnConsultarSeminario.TabIndex = 8;
            this.btnConsultarSeminario.Text = "Consultar Seminarios";
            this.btnConsultarSeminario.UseVisualStyleBackColor = true;
            this.btnConsultarSeminario.Click += new System.EventHandler(this.btnConsultarSeminario_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-3, 659);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1071, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "Todos los derechos reservados - UTEC\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::Botones.Properties.Resources.banner;
            this.pictureBox1.Location = new System.Drawing.Point(0, -2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1068, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // Estudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.btnConsultarSeminario);
            this.Controls.Add(this.btnInscribirSeminario);
            this.Controls.Add(this.btnSeminariosInscritos);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Estudiante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estudiante";
            this.Load += new System.EventHandler(this.Estudiante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Button btnConsultarSeminario;
        private System.Windows.Forms.Button btnInscribirSeminario;
        private System.Windows.Forms.Button btnSeminariosInscritos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}