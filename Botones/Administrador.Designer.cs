﻿namespace Botones
{
    partial class Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Administrador));
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAgregarDocente = new System.Windows.Forms.Button();
            this.btnAgregarEstudiante = new System.Windows.Forms.Button();
            this.btnAgregarSeminario = new System.Windows.Forms.Button();
            this.btnProgramarSeminario = new System.Windows.Forms.Button();
            this.btnListado_Seminarios = new System.Windows.Forms.Button();
            this.btnActualizarSeminario = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrarSesion.Image = global::Botones.Properties.Resources.keyboard_return;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(403, 588);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(276, 49);
            this.btnCerrarSesion.TabIndex = 7;
            this.btnCerrarSesion.Text = "Cerrar sesión";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-5, 658);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1071, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "Todos los derechos reservados - UTEC\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::Botones.Properties.Resources.banner;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1068, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Image = global::Botones.Properties.Resources.baseline_support_agent_black_48dp;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(71, 509);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(375, 49);
            this.button1.TabIndex = 8;
            this.button1.Text = "Agregar administrador";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAgregarDocente
            // 
            this.btnAgregarDocente.Image = global::Botones.Properties.Resources.baseline_person_add_black_48dp;
            this.btnAgregarDocente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarDocente.Location = new System.Drawing.Point(550, 410);
            this.btnAgregarDocente.Name = "btnAgregarDocente";
            this.btnAgregarDocente.Size = new System.Drawing.Size(375, 49);
            this.btnAgregarDocente.TabIndex = 6;
            this.btnAgregarDocente.Text = "Agregar docente";
            this.btnAgregarDocente.UseVisualStyleBackColor = true;
            this.btnAgregarDocente.Click += new System.EventHandler(this.btnAgregarDocente_Click);
            // 
            // btnAgregarEstudiante
            // 
            this.btnAgregarEstudiante.Image = global::Botones.Properties.Resources.baseline_people_black_48dp;
            this.btnAgregarEstudiante.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarEstudiante.Location = new System.Drawing.Point(71, 410);
            this.btnAgregarEstudiante.Name = "btnAgregarEstudiante";
            this.btnAgregarEstudiante.Size = new System.Drawing.Size(375, 49);
            this.btnAgregarEstudiante.TabIndex = 5;
            this.btnAgregarEstudiante.Text = "Agregar estudiante";
            this.btnAgregarEstudiante.UseVisualStyleBackColor = true;
            this.btnAgregarEstudiante.Click += new System.EventHandler(this.btnAgregarEstudiante_Click);
            // 
            // btnAgregarSeminario
            // 
            this.btnAgregarSeminario.Image = global::Botones.Properties.Resources.auto_stories2;
            this.btnAgregarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarSeminario.Location = new System.Drawing.Point(71, 142);
            this.btnAgregarSeminario.Name = "btnAgregarSeminario";
            this.btnAgregarSeminario.Size = new System.Drawing.Size(375, 49);
            this.btnAgregarSeminario.TabIndex = 4;
            this.btnAgregarSeminario.Text = "Agregar seminario";
            this.btnAgregarSeminario.UseVisualStyleBackColor = true;
            this.btnAgregarSeminario.Click += new System.EventHandler(this.btnAgregarSeminario_Click);
            // 
            // btnProgramarSeminario
            // 
            this.btnProgramarSeminario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProgramarSeminario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnProgramarSeminario.Image = global::Botones.Properties.Resources.baseline_note_add_black_48dp;
            this.btnProgramarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProgramarSeminario.Location = new System.Drawing.Point(550, 142);
            this.btnProgramarSeminario.Name = "btnProgramarSeminario";
            this.btnProgramarSeminario.Size = new System.Drawing.Size(375, 49);
            this.btnProgramarSeminario.TabIndex = 18;
            this.btnProgramarSeminario.Text = "Programar seminario";
            this.btnProgramarSeminario.UseVisualStyleBackColor = true;
            this.btnProgramarSeminario.Click += new System.EventHandler(this.btnProgramarSeminario_Click);
            // 
            // btnListado_Seminarios
            // 
            this.btnListado_Seminarios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListado_Seminarios.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListado_Seminarios.Image = global::Botones.Properties.Resources.baseline_list_alt_black_48dp;
            this.btnListado_Seminarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListado_Seminarios.Location = new System.Drawing.Point(71, 232);
            this.btnListado_Seminarios.Name = "btnListado_Seminarios";
            this.btnListado_Seminarios.Size = new System.Drawing.Size(375, 54);
            this.btnListado_Seminarios.TabIndex = 19;
            this.btnListado_Seminarios.Text = "Listado de seminarios programados";
            this.btnListado_Seminarios.UseVisualStyleBackColor = true;
            this.btnListado_Seminarios.Click += new System.EventHandler(this.btnListado_Seminarios_Click);
            // 
            // btnActualizarSeminario
            // 
            this.btnActualizarSeminario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizarSeminario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnActualizarSeminario.Image = global::Botones.Properties.Resources.baseline_edit_black_48dp;
            this.btnActualizarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizarSeminario.Location = new System.Drawing.Point(550, 232);
            this.btnActualizarSeminario.Name = "btnActualizarSeminario";
            this.btnActualizarSeminario.Size = new System.Drawing.Size(375, 54);
            this.btnActualizarSeminario.TabIndex = 20;
            this.btnActualizarSeminario.Text = "Actualizar seminario programado";
            this.btnActualizarSeminario.UseVisualStyleBackColor = true;
            this.btnActualizarSeminario.Click += new System.EventHandler(this.btnActualizarSeminario_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(398, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 26);
            this.label1.TabIndex = 21;
            this.label1.Text = "Administración de usuarios";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(398, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(298, 26);
            this.label3.TabIndex = 22;
            this.label3.Text = "Administración de seminarios";
            // 
            // Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnActualizarSeminario);
            this.Controls.Add(this.btnListado_Seminarios);
            this.Controls.Add(this.btnProgramarSeminario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.btnAgregarDocente);
            this.Controls.Add(this.btnAgregarEstudiante);
            this.Controls.Add(this.btnAgregarSeminario);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Administrador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrador";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Button btnAgregarDocente;
        private System.Windows.Forms.Button btnAgregarEstudiante;
        private System.Windows.Forms.Button btnAgregarSeminario;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnProgramarSeminario;
        private System.Windows.Forms.Button btnListado_Seminarios;
        private System.Windows.Forms.Button btnActualizarSeminario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}