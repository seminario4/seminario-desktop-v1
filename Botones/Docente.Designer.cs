﻿namespace Botones
{
    partial class Docente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Docente));
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnListado_Seminarios = new System.Windows.Forms.Button();
            this.btnActualizarSeminario = new System.Windows.Forms.Button();
            this.btnProgramarSeminario = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarSesion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCerrarSesion.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrarSesion.Image = global::Botones.Properties.Resources.keyboard_return;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(342, 463);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(383, 54);
            this.btnCerrarSesion.TabIndex = 6;
            this.btnCerrarSesion.Text = "Cerrar Sesion";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-3, 659);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1071, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "Todos los derechos reservados - UTEC\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::Botones.Properties.Resources.banner;
            this.pictureBox1.Location = new System.Drawing.Point(0, -2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1068, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // btnListado_Seminarios
            // 
            this.btnListado_Seminarios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListado_Seminarios.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListado_Seminarios.Image = global::Botones.Properties.Resources.baseline_list_alt_black_48dp;
            this.btnListado_Seminarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListado_Seminarios.Location = new System.Drawing.Point(293, 279);
            this.btnListado_Seminarios.Name = "btnListado_Seminarios";
            this.btnListado_Seminarios.Size = new System.Drawing.Size(482, 54);
            this.btnListado_Seminarios.TabIndex = 7;
            this.btnListado_Seminarios.Text = "Listado de Seminarios";
            this.btnListado_Seminarios.UseVisualStyleBackColor = true;
            this.btnListado_Seminarios.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnActualizarSeminario
            // 
            this.btnActualizarSeminario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizarSeminario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnActualizarSeminario.Image = global::Botones.Properties.Resources.baseline_edit_black_48dp;
            this.btnActualizarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizarSeminario.Location = new System.Drawing.Point(293, 371);
            this.btnActualizarSeminario.Name = "btnActualizarSeminario";
            this.btnActualizarSeminario.Size = new System.Drawing.Size(482, 54);
            this.btnActualizarSeminario.TabIndex = 5;
            this.btnActualizarSeminario.Text = "Actualizar Seminario";
            this.btnActualizarSeminario.UseVisualStyleBackColor = true;
            this.btnActualizarSeminario.Click += new System.EventHandler(this.btnActualizarSeminario_Click);
            // 
            // btnProgramarSeminario
            // 
            this.btnProgramarSeminario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProgramarSeminario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnProgramarSeminario.Image = global::Botones.Properties.Resources.baseline_note_add_black_48dp;
            this.btnProgramarSeminario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProgramarSeminario.Location = new System.Drawing.Point(293, 187);
            this.btnProgramarSeminario.Name = "btnProgramarSeminario";
            this.btnProgramarSeminario.Size = new System.Drawing.Size(482, 54);
            this.btnProgramarSeminario.TabIndex = 4;
            this.btnProgramarSeminario.Text = "Programar Seminario";
            this.btnProgramarSeminario.UseVisualStyleBackColor = true;
            this.btnProgramarSeminario.Click += new System.EventHandler(this.btnProgramarSeminario_Click);
            // 
            // Docente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnListado_Seminarios);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.btnActualizarSeminario);
            this.Controls.Add(this.btnProgramarSeminario);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Docente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Docente";
            this.Load += new System.EventHandler(this.Docente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Button btnActualizarSeminario;
        private System.Windows.Forms.Button btnProgramarSeminario;
        private System.Windows.Forms.Button btnListado_Seminarios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}