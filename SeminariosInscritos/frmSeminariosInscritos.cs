﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace SeminariosInscritos
{
    public partial class frmSeminariosInscritos : Form
    {
        Negocios n = new Negocios();
        public frmSeminariosInscritos()
        {
            InitializeComponent();
        }
        public string  usuario { get; set; }
        private void frmSeminariosInscritos_Load(object sender, EventArgs e)
        {
            n.nombre_usuario = this.usuario;
            var sem = n.ver_sem_inscritos();
            if (sem.Rows.Count>0)
            {
                for (int i = 0; i < sem.Rows.Count; i++)
                {
                    string carnet =sem.Rows[i][0].ToString();
                    string cod_sem = sem.Rows[i][1].ToString();
                    string nom_sem = sem.Rows[i][2].ToString();
                    string cod_mate = sem.Rows[i][3].ToString();
                    string fecha = sem.Rows[i][4].ToString();
                    dgvSeminarios.Rows.Add(carnet,cod_sem,nom_sem,cod_mate,fecha);
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
