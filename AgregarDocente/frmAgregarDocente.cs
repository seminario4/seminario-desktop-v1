﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace AgregarDocente
{
    public partial class frmAgregarDocente : Form
    {
        public frmAgregarDocente()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (tbxUsuario.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(tbxUsuario,"Campo Requerido");  
            }
            else if (tbxUsuario.Text.Trim().Contains(" ")==true)
            {
                errorProvider1.SetError(tbxUsuario,"No se permiten espacios en blanco");
            }
            else if (tbxContraseña.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(tbxContraseña, "Campo Requerido");  
            }
            else if (tbxNombre.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(tbxNombre, "Campo Requerido");  
            }
            else if(tbxApellido.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(tbxApellido, "Campo Requerido");  
            }
            else if (tbxCorreo.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(tbxCorreo, "Campo Requerido");  
            }
            else if (cbxFacultad.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(cbxFacultad,"Seleccione la facultad");
            }
            else
            {
                string ElUsuario = tbxUsuario.Text.Trim();
                n.nombre_usuario = ElUsuario;
                int res = n.verificarUSuario();
                if (res==1)
                {
                    MessageBox.Show("El nombre de usuario ya exite");
                    tbxUsuario.Text = String.Empty;
                    tbxUsuario.Focus();
                }
                else
                {
                    string ElNombre = tbxNombre.Text.Trim();
                    
                    string ElCorreo = tbxCorreo.Text.Trim();
                    string LaContra = tbxContraseña.Text.Trim();
                    string ElApellido = tbxApellido.Text.Trim();
                   
                    var idFac = cbxFacultad.SelectedValue.ToString();
                    int idfacultad = int.Parse(idFac);

                    n.nombre_usuario = ElUsuario;
                    n.pwd = LaContra;
                    n.nombres = ElNombre.ToUpper();
                    n.apellidos = ElApellido.ToUpper();
                    n.id_facultad = idfacultad;
                    n.correo = ElCorreo;

                    bool resp = n.agregar_docente();
                    if (resp)
                    {
                         MessageBox.Show("Guardado Existosamente");
                         tbxNombre.Text = String.Empty;
                         tbxCorreo.Text = String.Empty;
                         tbxContraseña.Text = String.Empty;
                         tbxUsuario.Text = String.Empty;
                         tbxApellido.Text = String.Empty;
                         cbxFacultad.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("Algo Salio mal");
                    }

                }
                
                
            }
            

            
        }

        private void tbxUsuario_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(tbxUsuario, string.Empty);
        }

        private void tbxNombre_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(tbxNombre, string.Empty);
        }

        private void tbxCorreo_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(tbxCorreo, string.Empty);
        }

        private void tbxContraseña_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(tbxContraseña, string.Empty);
        }

        private void tbxApellido_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(tbxApellido, string.Empty);
        }

        private void cbxFacultad_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbxFacultad,String.Empty); 
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void frmRegistroDocente_Load(object sender, EventArgs e)
        {
            tbxUsuario.Focus();
            cbxFacultad.DataSource = n.listarFacultades();
            cbxFacultad.DisplayMember = "NOM_FACULTAD";
            cbxFacultad.ValueMember = "ID_FACULTAD";
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxFacultad.SelectedIndex = -1;
        }
    }
}
