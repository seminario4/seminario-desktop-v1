﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace ConsultarSeminarios
{
    public partial class frmConsultarSeminarios : Form
    {
        public frmConsultarSeminarios()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        private void tbxCodSeminario_Validated(object sender, EventArgs e)
        {
            if (txtCodSeminario.Text.Trim() == "")
            {
                errorProvid.SetError(txtCodSeminario, "Ingrese Codigo de Seminario");
                txtCodSeminario.Focus();
            }
            else
            {
                errorProvid.Clear();
            }

            errorProvid.SetError(txtCodSeminario, string.Empty);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {


        }

        private void frmConsultarSeminarios_Load(object sender, EventArgs e)
        {
            var table = n.listarSeminarios();
            if (table.Rows.Count > 0)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    string codigo = table.Rows[i]["COD_SEMINARIO"].ToString();
                    string nombre = table.Rows[i]["NOM_SEMINARIO"].ToString();
                    string horario = table.Rows[i]["HORARIO"].ToString().ToString();
                    string cupos = table.Rows[i]["CUPOS"].ToString();
                    string valor = table.Rows[i]["VALOR"].ToString();
                    dgvSeminarios.Rows.Add(codigo, nombre, horario, cupos, valor);
                }

            }
        }

        private void txtCodSeminario_TextChanged(object sender, EventArgs e)
        {
            if (txtCodSeminario.Text.Trim().Length > 0)
            {
                dgvSeminarios.Rows.Clear();
                n.cod_Seminario = txtCodSeminario.Text.Trim();
                var table = n.ConsultaSeminarios();
                if (table.Rows.Count > 0)
                {
                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        string codigo = table.Rows[i]["COD_SEMINARIO"].ToString();
                        string nombre = table.Rows[i]["NOM_SEMINARIO"].ToString();
                        string horario = table.Rows[i]["HORARIO"].ToString().ToString();
                        string cupos = table.Rows[i]["CUPOS"].ToString();
                        string valor = table.Rows[i]["VALOR"].ToString();
                        dgvSeminarios.Rows.Add(codigo, nombre, horario, cupos, valor);
                    }
                }
            }
            else
            {
                dgvSeminarios.Rows.Clear();
                var table2 = n.listarSeminarios();
                if (table2.Rows.Count > 0)
                {
                    for (int i = 0; i < table2.Rows.Count; i++)
                    {
                        string codigo = table2.Rows[i]["COD_SEMINARIO"].ToString();
                        string nombre = table2.Rows[i]["NOM_SEMINARIO"].ToString();
                        string horario = table2.Rows[i]["HORARIO"].ToString().ToString();
                        string cupos = table2.Rows[i]["CUPOS"].ToString();
                        dgvSeminarios.Rows.Add(codigo, nombre, horario, cupos);
                    }
                }

            }
        }

        

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void txtCodSeminario_KeyPress(object sender, KeyPressEventArgs e)
        {
            

            }
        }
    }
