﻿namespace ConsultarSeminarios
{
    partial class frmConsultarSeminarios
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultarSeminarios));
            this.lblCodSeminario = new System.Windows.Forms.Label();
            this.txtCodSeminario = new System.Windows.Forms.TextBox();
            this.dgvSeminarios = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomSeminario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Horarios = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cupos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProvid = new System.Windows.Forms.ErrorProvider(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeminarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCodSeminario
            // 
            this.lblCodSeminario.AutoSize = true;
            this.lblCodSeminario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodSeminario.ForeColor = System.Drawing.Color.White;
            this.lblCodSeminario.Location = new System.Drawing.Point(32, 117);
            this.lblCodSeminario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodSeminario.Name = "lblCodSeminario";
            this.lblCodSeminario.Size = new System.Drawing.Size(201, 25);
            this.lblCodSeminario.TabIndex = 2;
            this.lblCodSeminario.Text = "Codigo de Seminario:";
            // 
            // txtCodSeminario
            // 
            this.txtCodSeminario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodSeminario.Location = new System.Drawing.Point(241, 114);
            this.txtCodSeminario.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodSeminario.Name = "txtCodSeminario";
            this.txtCodSeminario.Size = new System.Drawing.Size(301, 30);
            this.txtCodSeminario.TabIndex = 5;
            this.txtCodSeminario.TextChanged += new System.EventHandler(this.txtCodSeminario_TextChanged);
            this.txtCodSeminario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodSeminario_KeyPress);
            this.txtCodSeminario.Validated += new System.EventHandler(this.tbxCodSeminario_Validated);
            // 
            // dgvSeminarios
            // 
            this.dgvSeminarios.AllowUserToAddRows = false;
            this.dgvSeminarios.AllowUserToDeleteRows = false;
            this.dgvSeminarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSeminarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.NomSeminario,
            this.Horarios,
            this.Cupos,
            this.valor});
            this.dgvSeminarios.Location = new System.Drawing.Point(67, 171);
            this.dgvSeminarios.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSeminarios.Name = "dgvSeminarios";
            this.dgvSeminarios.ReadOnly = true;
            this.dgvSeminarios.RowHeadersWidth = 51;
            this.dgvSeminarios.Size = new System.Drawing.Size(928, 309);
            this.dgvSeminarios.TabIndex = 4;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.MinimumWidth = 6;
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            this.Codigo.Width = 125;
            // 
            // NomSeminario
            // 
            this.NomSeminario.HeaderText = "Nombre de Seminario";
            this.NomSeminario.MinimumWidth = 6;
            this.NomSeminario.Name = "NomSeminario";
            this.NomSeminario.ReadOnly = true;
            this.NomSeminario.Width = 300;
            // 
            // Horarios
            // 
            this.Horarios.HeaderText = "Horarios";
            this.Horarios.MinimumWidth = 6;
            this.Horarios.Name = "Horarios";
            this.Horarios.ReadOnly = true;
            this.Horarios.Width = 200;
            // 
            // Cupos
            // 
            this.Cupos.HeaderText = "Cupos";
            this.Cupos.MinimumWidth = 6;
            this.Cupos.Name = "Cupos";
            this.Cupos.ReadOnly = true;
            this.Cupos.Width = 125;
            // 
            // valor
            // 
            this.valor.HeaderText = "valor";
            this.valor.MinimumWidth = 6;
            this.valor.Name = "valor";
            this.valor.ReadOnly = true;
            this.valor.Width = 125;
            // 
            // errorProvid
            // 
            this.errorProvid.ContainerControl = this;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 645);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1061, 28);
            this.label2.TabIndex = 15;
            this.label2.Text = "Todos los derechos reservados - UTEC\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImage = global::ConsultarSeminarios.Properties.Resources.banner2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1062, 90);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Image = global::ConsultarSeminarios.Properties.Resources.keyboard_return;
            this.btnCerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrar.Location = new System.Drawing.Point(399, 516);
            this.btnCerrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(252, 35);
            this.btnCerrar.TabIndex = 1;
            this.btnCerrar.Text = "Regresar a Menú";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // frmConsultarSeminarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1062, 673);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dgvSeminarios);
            this.Controls.Add(this.txtCodSeminario);
            this.Controls.Add(this.lblCodSeminario);
            this.Controls.Add(this.btnCerrar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmConsultarSeminarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar  Seminarios";
            this.Load += new System.EventHandler(this.frmConsultarSeminarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeminarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Label lblCodSeminario;
        private System.Windows.Forms.TextBox txtCodSeminario;
        private System.Windows.Forms.DataGridView dgvSeminarios;
        private System.Windows.Forms.ErrorProvider errorProvid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomSeminario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Horarios;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cupos;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
    }
}

