﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using CapaNegocios;
namespace ImprimirListado
{
    public partial class Listado : Form
    {
        public Listado()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        public int id_facultad { get; set; }
        public int id_Sem_prog { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        
        public void GenerarDocumentos(Document document)
        {
            //se crea un objeto PdfTable con el numero de columnas del dataGridView 
            PdfPTable datatable = new PdfPTable(dtListado.ColumnCount);

            //asignamos algunas propiedades para el diseño del pdf 
            datatable.DefaultCell.Padding = 1;
            float[] headerwidths = GetTamañoColumnas(dtListado);

            datatable.SetWidths(headerwidths);
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 1;

            //DEFINIMOS EL COLOR DE LAS CELDAS EN EL PDF
            datatable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;

            //DEFINIMOS EL COLOR DE LOS BORDES
            datatable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;

            //LA FUENTE DE NUESTRO TEXTO
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA);

            Phrase objP = new Phrase("A", fuente);

            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            //SE GENERA EL ENCABEZADO DE LA TABLA EN EL PDF 
            for (int i = 0; i < dtListado.ColumnCount; i++)
            {

                objP = new Phrase(dtListado.Columns[i].HeaderText, fuente);
                datatable.HorizontalAlignment = Element.ALIGN_CENTER;

                datatable.AddCell(objP);

            }
            datatable.HeaderRows = 1;

            datatable.DefaultCell.BorderWidth = 1;

            //SE GENERA EL CUERPO DEL PDF
            for (int i = 0; i < dtListado.RowCount; i++)
            {
                for (int j = 0; j < dtListado.ColumnCount; j++)
                {
                    objP = new Phrase(dtListado[j, i].Value.ToString(), fuente);
                    datatable.AddCell(objP);
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }

        //Función que obtiene los tamaños de las columnas del datagridview
        public float[] GetTamañoColumnas(DataGridView dg)
        {
            //Tomamos el numero de columnas
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount; i++)
            {
                //Tomamos el ancho de cada columna
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (dtListado.Rows.Count<1)
            {
                MessageBox.Show("No Hay Datos Para Imprimir");
            }
            else
            {    //ESCOJE A RUTA DONDE GUARDAREMOS EL PDF
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF Files (*.pdf)|.pdf|All Files (*.*)|*.*";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    string filename = save.FileName;
                    Document doc = new Document(PageSize.A3, 30, 30, 30, 30);
                    Chunk encab = new Chunk(this.codigo+" "+this.nombre, FontFactory.GetFont("COURIER", 18));

                    try
                    {
                        FileStream file = new FileStream(filename, FileMode.OpenOrCreate);
                        PdfWriter writer = PdfWriter.GetInstance(doc, file);
                        writer.ViewerPreferences = PdfWriter.PageModeUseThumbs;
                        writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn;
                        doc.Open();

                        doc.Add(new Paragraph(encab));
                        GenerarDocumentos(doc);

                        Process.Start(filename);
                        doc.Close();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void Listado_Load(object sender, EventArgs e)
        {
            
            n.id_SeminarioPROG = this.id_Sem_prog;
            n.id_facultad = this.id_facultad;
            var tabla = n.listado_imprimir();
            if (tabla.Rows.Count>0)
            {
                int numero = 1;
                for (int i = 0; i < tabla.Rows.Count; i++)
                {
                    string carnet =tabla.Rows[i][0].ToString();
                    string nombres =tabla.Rows[i][1].ToString();
                    string apellidos =tabla.Rows[i][2].ToString();
                    dtListado.Rows.Add(numero, carnet, nombres + " " + apellidos, "  ");
                    numero++;
                }
               
            }
            
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
        
    }
}
