﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;
using System.Data;
using System.Windows.Forms;
namespace CapaNegocios
{
    public class Negocios
    {
        CConexion cn = new CConexion();
        public int Clave_carrera { get; set; }
        public string nombre_usuario { get; set; }
        public string pwd { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string correo { get; set; }
        public string cod_Seminario { get; set; }
        public string nombreSeminario { get; set; }
        public int id_facultad { get; set; }
        public string carnet { get; set; }
        public string nombre_carrera { get; set; }
        public int id_edificio { get; set; }
        public int id_seminario { get; set; }
        public int id_docente { get; set; }
        public string fecha { get; set; }
        public string hora_i { get; set; }
        public string hora_f { get; set; }
        public int id_aula { get; set; }
        public int cupos { get; set; }
        public string nombre_aula { get; set; }
        public int cupo_ante { get; set; }
        public int nuevo_cupo { get; set; }
        public int id_SeminarioPROG { get; set; }
        public string cod_materia { get; set; }
        public string valo_seminario { get; set; }
        public DataTable inicioSesion()
        {
            DataTable dt = new DataTable();
            string consulta = "exec inicioSesion '" + nombre_usuario + "'";
            dt = cn.consultalvl2(consulta);
            return dt;

        }

        public DataTable listarFacultades()
        {
            DataTable tabla = new DataTable();
            string consulta = "EXEC LISTAR_FACULTADES";
            tabla = cn.consultalvl2(consulta);
            return tabla;
        }
        public DataTable listarcarreras(int id)
        {
            DataTable tabla = new DataTable();
            string consulta = "EXEC CARRERA_POR_FACULTAD " + id;
            tabla = cn.consultalvl2(consulta);
            return tabla;
        }
        public int verificar_existenciaSeminario()
        {
            int res = 0;
            try
            {
                string consulta = "EXEC VERIFICAR_COD_SEMINARIO '" + cod_Seminario + "'";
                var tba = cn.consultalvl2(consulta);
                res = int.Parse(tba.Rows[0][0].ToString());
                return res;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return res;
            }
         
        }
        public bool agregarSeminario()
        {
            string consulta = "EXEC AGREGAR_SEMINARIO '"+nombreSeminario+"','"+cod_Seminario+"','"+id_facultad+"','"+ valo_seminario+"'";
            return cn.Consulta(consulta);
        }
        public bool agregarAlumno()
        {
            string consulta = "EXEC Registra_Alumno '"+nombre_usuario+"','"+pwd+"','"+nombre_carrera+"',"+id_facultad+",'"+carnet+"','"+correo+"','"+nombres+"','"+apellidos+"' ";
            return cn.Consulta(consulta);
        }
        public int verificarUSuario()
        {
            string consulta="EXEC VERIFICAR_USUARIO '"+nombre_usuario+"'";
            var tab= cn.consultalvl2(consulta);
            return  int.Parse(tab.Rows[0][0].ToString());
        }
        public bool agregar_docente()
        {
            string consulta = "exec REGISTRAR_DOCENTE '" + nombre_usuario + "','" + pwd + "','" + nombres + "','" + apellidos + "','" + correo + "'," + id_facultad;
            return cn.Consulta(consulta);
        }
        public bool Registrar_Admin()
        {
            string consulta = "exec REGISTRAR_ADMINISTRADOR '"+nombre_usuario+"','"+pwd+"','"+nombres+"','"+apellidos+"','"+correo+"'";
            return cn.Consulta(consulta);
        }

        public DataTable listarEdif()
        {
            string consulta = "exec LISTAR_EDIFICIO";
            return cn.consultalvl2(consulta);
        }
        public DataTable listarAulas ()
        {
            string consulta = "exec LISTAR_AULA_POR_EDIF " + id_edificio;
            return cn.consultalvl2(consulta);
        }
        public DataTable ConsultarSeminarios()
        {
            string consulta = "exec listarSeminario_por_facultad " + id_facultad;
            return cn.consultalvl2(consulta); 
        }
        public DataTable ConsultaSeminarios()
        {
            string consulta = "exec CONSULTA_SEMINARIO '" + cod_Seminario+"'";
            return cn.consultalvl2(consulta);
        }
        public DataTable listarDOCENTE()
        {
            string consulta = "exec listarDocente_por_Facultad " + id_facultad;
            return cn.consultalvl2(consulta);
        }
        public void marcar()
        {
            string consulta = "EXEC MARCAR '"+nombre_usuario+"'";
            cn.Consulta(consulta);
        }
        public int idfacultad() 
        {
            string consulta = "EXEC VER_FACULTAD_POR_USUARIO '"+nombre_usuario+"'";
            var tab = cn.consultalvl2(consulta);
            return int.Parse(tab.Rows[0]["ID_FACULTAD"].ToString());

        }
        public DataTable listarSeminarios()
        {
            string consulta = "EXEC LISTAR_SEMINARIO";
            DataTable tb= cn.consultalvl2(consulta);
            return tb;
        }
        public bool programar_seminario()
        {
            string consulta = "EXEC PROGRAMAR_SEMINARIO "+id_seminario+","+id_facultad+",'"+fecha+"','"+hora_i+"','"+hora_f+"',"+id_aula+","+cupos+","+id_docente+"";
            return cn.Consulta(consulta);
        }
        public DataTable vericar_seminario()
        {
            string consula = "EXEC COMPROBAR_SEMINARIO '"+hora_f+"','"+hora_i+"','"+fecha+"',"+id_aula+","+id_docente+"";
            return cn.consultalvl2(consula);
        }
        public DataTable vericar_seminario2()
        {
            string consula = "EXEC COMPROBAR_SEMINARIO_ACTUALIZAR '" + hora_f + "','" + hora_i + "','" + fecha + "'," + id_aula + "," + id_docente + ","+id_SeminarioPROG;
            return cn.consultalvl2(consula);
        }
        public int obtener_idAula()
        {
            string consulta = "SELECT dbo.FX_ID_AULA("+id_edificio+",'"+nombre_aula+"') AS ID_AULA";
            var ta=cn.consultalvl2(consulta);
            int id = int.Parse(ta.Rows[0][0].ToString());
            return id;
        }
        public DataTable listardoActualizable()
        {
            string consulta = "EXEC LISTAR_ACTUALIZAR " + id_facultad;
            return cn.consultalvl2(consulta);
        }
        public bool ActualizarSeminario()
        {
            string consulta = "EXEC ACTUALIZAR_SEMINARIO " + id_SeminarioPROG + ",'" + fecha + "','" + hora_i + "','" + hora_f + "'," + id_aula + "," + cupos + "," + id_docente;
            return cn.Consulta(consulta);
        }
        public int comprobar_cupos()
        {
            string consulta = "SELECT dbo.fx_COMPROBAR_CUPOS( "+cupo_ante+","+nuevo_cupo+","+id_SeminarioPROG+") as RESP";
            //SELECT dbo.fx_COMPROBAR_CUPOS(120,1,23) AS RESP
            var tab= cn.consultalvl2(consulta);
            int res = int.Parse(tab.Rows[0][0].ToString());
            return res;
        }
        public DataTable listarMaterias()
        {
            string consulta = "SELECT MATERIAS.NOM_MATERIA,MATERIAS.COD_MATERIA FROM CARRERA INNER JOIN MATERIAS ON CARRERA.ID_CARRERA = MATERIAS.ID_CARRERA WHERE CARRERA.CODIGO=" + Clave_carrera;
            return cn.consultalvl2(consulta);
        }
        public DataTable listarSeminariosParaInscribir()
        {
            string consulta = "EXEC LISTAR_SEMINARIO_INSCRIBIR "+id_seminario;
            return cn.consultalvl2(consulta);
        }
        public DataTable verificar_pagos()
        {
            string consulta = "VERIFICAR_PAGOS "+"'"+nombre_usuario+"'";
            return cn.consultalvl2(consulta);
        }
        public bool inscribir_Seminario()
        {
            string consulta = "exec REGISTRAR_INSCRIPCION "+id_SeminarioPROG+","+id_seminario+",'"+nombre_usuario+"','"+cod_materia+"' ";
            return cn.Consulta(consulta);
        }
        public DataTable ver_sem_inscritos()
        {
            string consulta = "EXEC VER_SEMINARIOS_INSCRIOS '"+nombre_usuario+"'";
            return cn.consultalvl2(consulta);
        }
        public DataTable ver_sem_inscrito_por_aulum()
        {
            string consulta = "exec VERIFICAR_SI_EXISTE_EN_SEMIMARIO_PROGRAMADOS '"+nombre_usuario+"',"+id_SeminarioPROG;
            return cn.consultalvl2(consulta);
            
        }

        public DataTable LISTADO_SEMPROGRAMADOS_POR_FACULTAD()
        {
            string consulta = "EXEC LISTADO_SEMPROGRAMADOS_POR_FACULTAD "+id_facultad;
            return cn.consultalvl2(consulta);
        }
        public DataTable LISTADO_SEMPROGRAMADOS_POR_FACULTAD_AND_CODIGO_SEM()
        {
            string consulta = "LISTADO_SEMPROGRAMADOS_POR_FACULTAD_AND_CODIGO_SEM "+id_facultad+",'"+cod_Seminario+"'";
            return cn.consultalvl2(consulta);
        }
        public DataTable listado_imprimir()
        {
            string consulta = "EXEC LISTADO_IMPRIMIR "+id_SeminarioPROG;
            return  cn.consultalvl2(consulta);
        }
    }
}
