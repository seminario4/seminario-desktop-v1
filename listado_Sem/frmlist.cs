﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
using ImprimirListado;
namespace listado_Sem
{
    public partial class frmlist : Form
    {
        public frmlist()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        public int id_facultad { get; set; }
        private void frmlist_Load(object sender, EventArgs e)
        {
            n.id_facultad = this.id_facultad;
            var tabla = n.LISTADO_SEMPROGRAMADOS_POR_FACULTAD();
            if (tabla.Rows.Count > 0)
            {
                for (int i = 0; i < tabla.Rows.Count; i++)
                {
                    string cod_sem = tabla.Rows[i][0].ToString();
                    string nom_sem = tabla.Rows[i][1].ToString();
                    string horario = tabla.Rows[i][2].ToString();
                    string cupos = tabla.Rows[i][3].ToString();
                    string id_sem_prog = tabla.Rows[i][4].ToString();
                    dgvListado.Rows.Add(cod_sem, nom_sem, horario, cupos, "Generar Lista", id_sem_prog);
                }

            }
        }

        private void txtCodigoSeminario_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoSeminario.Text.Trim().Length > 0)
            {
                dgvListado.Rows.Clear();
                n.cod_Seminario = txtCodigoSeminario.Text.Trim();
                var tabla = n.LISTADO_SEMPROGRAMADOS_POR_FACULTAD_AND_CODIGO_SEM();
                if (tabla.Rows.Count > 0)
                {
                    for (int i = 0; i < tabla.Rows.Count; i++)
                    {
                        string cod_sem = tabla.Rows[i][0].ToString();
                        string nom_sem = tabla.Rows[i][1].ToString();
                        string horario = tabla.Rows[i][2].ToString();
                        string cupos = tabla.Rows[i][3].ToString();
                        string id_sem_prog = tabla.Rows[i][4].ToString();
                        dgvListado.Rows.Add(cod_sem, nom_sem, horario, cupos, "Generar Lista", id_sem_prog);
                    }
                }
            }
            else
            {
                dgvListado.Rows.Clear();
                var tabla2 = n.LISTADO_SEMPROGRAMADOS_POR_FACULTAD();
                if (tabla2.Rows.Count > 0)
                {
                    for (int i = 0; i < tabla2.Rows.Count; i++)
                    {
                        string cod_sem = tabla2.Rows[i][0].ToString();
                        string nom_sem = tabla2.Rows[i][1].ToString();
                        string horario = tabla2.Rows[i][2].ToString();
                        string cupos = tabla2.Rows[i][3].ToString();
                        string id_sem_prog = tabla2.Rows[i][4].ToString();
                        dgvListado.Rows.Add(cod_sem, nom_sem, horario, cupos, "Generar Lista", id_sem_prog);
                    }
                }

            }
        }

        private void dgvListado_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dgvListado.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Generar Lista"))
                {

                    int id_semprog = int.Parse(dgvListado.Rows[e.RowIndex].Cells[5].Value.ToString());
                    string nombre_sem = dgvListado.Rows[e.RowIndex].Cells[1].Value.ToString();
                    string cod_Sem = dgvListado.Rows[e.RowIndex].Cells[0].Value.ToString();
                    Listado list = new Listado();
                    list.nombre = nombre_sem;
                    list.codigo = cod_Sem;
                    DialogResult res1;
                    list.id_Sem_prog = id_semprog;
                    list.id_facultad = this.id_facultad;
                    this.Visible = false;
                    res1 = list.ShowDialog();
                    if (res1 == DialogResult.Cancel)
                    {
                        this.Visible = true;
                        list.Dispose();

                    }
                   
                }
            }
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
