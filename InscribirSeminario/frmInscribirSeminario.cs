﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace InscribirSeminario
{
    public partial class frmInscribirSeminario : Form
    {
        Negocios n = new Negocios();
        public frmInscribirSeminario()
        {
            InitializeComponent();
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
        public string USUAR { get; set; }

        string codigo_sem_on, nombre_sem_on, fecha_on,cod_materia;
        int id_sem_prog, id_sem;
        private void frmInscribirSeminario_Load(object sender, EventArgs e)
        {
            n.Clave_carrera = int.Parse(this.USUAR.ToString().Substring(0, 2)) ;
            var tb= n.listarMaterias();
            cbxMaterias.DataSource = tb;
            cbxMaterias.DisplayMember = "NOM_MATERIA";
            cbxMaterias.ValueMember = "COD_MATERIA";
            cbxMaterias.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxMaterias.SelectedIndex = -1;
            cargador();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void cargador()
        {
            n.nombre_usuario = USUAR;
            var pagotab = n.verificar_pagos();
            if (pagotab.Rows.Count > 0)
            {
                for (int a = 0; a < pagotab.Rows.Count; a++)
                {
                    int id_Semi = int.Parse(pagotab.Rows[a][0].ToString());
                    n.id_seminario = id_Semi;
                    var tba = n.listarSeminariosParaInscribir();
                    if (tba.Rows.Count > 0)
                    {
                        for (int i = 0; i < tba.Rows.Count; i++)
                        {
                            string id_Sem_prog = tba.Rows[i][0].ToString();
                            string cod_Sem = tba.Rows[i][1].ToString();
                            string nombre_sem = tba.Rows[i][2].ToString();
                            string horario = tba.Rows[i][3].ToString();
                            string nombre_aula = tba.Rows[i][4].ToString();
                            string nombre_edif = tba.Rows[i][5].ToString();
                            string cupos = tba.Rows[i][6].ToString();
                            string id_sem = tba.Rows[i][7].ToString();

                            dgvSeminarios.Rows.Add(cod_Sem, nombre_sem, horario, nombre_aula, nombre_edif, cupos, "Seleccionar", id_Sem_prog, id_sem);
                        }
                    }
                }
            }
        }
        private void btnIsncribir_Click(object sender, EventArgs e)
        {
            n.id_SeminarioPROG = id_sem_prog;
            n.nombre_usuario = this.USUAR;
            var t = n.ver_sem_inscrito_por_aulum();
            int respbd;
            
            respbd = int.Parse(t.Rows[0][0].ToString());
           

               
            
           
            if (txtCod_sem.Text.Trim()==String.Empty)
            {
                MessageBox.Show("Primero seleccione un seminario");
            }
            else if(cbxMaterias.Text.Trim()==String.Empty)
            {
                errorProvider1.SetError(cbxMaterias,"Seleccione la materia");
            }
            else if (respbd==1)
            {
                MessageBox.Show("Usted ya se a Inscrito a Este Seminario \n PD: No hay Reembolso");
            }
            else
            {
              DialogResult res=  MessageBox.Show("Usted va a inscribir el Seminarios :" + codigo_sem_on + "\n Nombre : " + nombre_sem_on + "\n En el Horaio de : " + fecha_on + "\n \n ¿Esta Seguro de que quiere Inscribirlo?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
              if (res==DialogResult.No)
              {
                  txt_fecha_sem.Text = String.Empty;
                  txtCod_sem.Text = String.Empty;
                  txtNom_sem.Text = String.Empty;
                  cbxMaterias.SelectedIndex = -1;                  
              }
              else
              {
                  string cod_mat = cbxMaterias.SelectedValue.ToString();
                  n.id_SeminarioPROG = id_sem_prog;
                  n.id_seminario = id_sem;
                  n.nombre_usuario = this.USUAR;
                  n.cod_materia = cod_mat;

                  bool resp = n.inscribir_Seminario();
                  if (resp)
                  {
                      MessageBox.Show("Usted se a Inscrito de Forma Exitosa");
                      txt_fecha_sem.Text = String.Empty;
                      txtCod_sem.Text = String.Empty;
                      txtNom_sem.Text = String.Empty;
                      cbxMaterias.SelectedIndex = -1;
                      dgvSeminarios.Rows.Clear();
                      cargador();
                  }
                  else
                  {
                      MessageBox.Show("Algo Salió mal al Intertar Inscribir el Seminario"); 
                  }


              }
            }
            
        }

        private void cbxMaterias_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbxMaterias, String.Empty);
        }

        private void dgvSeminarios_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dgvSeminarios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Seleccionar"))
                {
                     codigo_sem_on = dgvSeminarios.Rows[e.RowIndex].Cells[0].Value.ToString();
                     nombre_sem_on = dgvSeminarios.Rows[e.RowIndex].Cells[1].Value.ToString();
                     fecha_on = dgvSeminarios.Rows[e.RowIndex].Cells[2].Value.ToString();
                     id_sem_prog = int.Parse(dgvSeminarios.Rows[e.RowIndex].Cells[7].Value.ToString());
                     id_sem=int.Parse(dgvSeminarios.Rows[e.RowIndex].Cells[8].Value.ToString());
                    txtCod_sem.Text = codigo_sem_on;
                    txtNom_sem.Text = nombre_sem_on;
                    txt_fecha_sem.Text = fecha_on;
                }
            }
        }
    }
}
