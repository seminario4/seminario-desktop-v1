﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace ProgramarSeminario
{
    public partial class frmProgramarSeminario : Form
    {
        public int id_facultad { get; set; }
        public frmProgramarSeminario()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios();
        DateTime localDate;
        private void FormProgSeminarios_Load(object sender, EventArgs e)
        {
            localDate = DateTime.Now;
            this.dtpFecha.MinDate = localDate;
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSeminario.DropDownStyle = ComboBoxStyle.DropDownList;

            cbAula.DropDownStyle = ComboBoxStyle.DropDownList;
            cbNombrePonente.DropDownStyle = ComboBoxStyle.DropDownList;

            dateTimePicker1.CustomFormat = "HH:mm";            
            cbEdificio.DropDownStyle = ComboBoxStyle.DropDownList; 
            cbxFacultad.DataSource = n.listarFacultades();
            cbxFacultad.DisplayMember = "NOM_FACULTAD";
            cbxFacultad.ValueMember = "ID_FACULTAD";
            cbxFacultad.Enabled = id_facultad == -1 ? true : false;
            cbxFacultad.SelectedIndex = 0;
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;

            cbxFacultad.Focus();


            cbxFacultad.SelectedIndex = id_facultad;


            cbEdificio.DataSource = n.listarEdif();
            cbEdificio.DisplayMember = "NOM_EDIF";
            cbEdificio.ValueMember = "ID_EDIFICIO";
            cbEdificio.SelectedIndex = 0;
            n.id_facultad = this.id_facultad;
            cbNombrePonente.DataSource = n.listarDOCENTE();
            cbNombrePonente.DisplayMember = "NOMBRE";
            cbNombrePonente.ValueMember = "ID_DOCENTE";
            cbNombrePonente.SelectedIndex = -1;

            cbSeminario.DataSource = n.ConsultarSeminarios();
            cbSeminario.DisplayMember = "nombre";
            cbSeminario.ValueMember = "ID_SEMINARIO";
            cbSeminario.SelectedIndex = -1;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int hora_i, hora_f, min_i, min_f;
            DateTime hora_inicio = Convert.ToDateTime(dateTimePicker1.Text.Trim());
            DateTime hora_final = Convert.ToDateTime(dateTimePicker2.Text.Trim());
            DateTime fecha = Convert.ToDateTime(dtpFecha.Text.Trim());
            int dia, mes, año;
            dia = fecha.Day;
            mes = fecha.Month;
            año = fecha.Year;
            hora_i=hora_inicio.Hour;
            hora_f=hora_final.Hour;
            min_f=hora_final.Minute;
            min_i=hora_inicio.Minute;
            
             if (cbSeminario.Text.Trim()==String.Empty)
            {
                MessageBox.Show("Ningun Seminario A sido agregado");
            }
            else  if (fecha < localDate)
             {
                 MessageBox.Show("Seleccione una Fecha mayor a la de hoy");
             }
             else if (hora_f - hora_i == 0)
             {
                 MessageBox.Show("Verifique la hora de inicio y la hora de salida \n Como minimo programe una hora");
                 dateTimePicker1.Focus();
             }
             else if (hora_f - hora_i < 0)
             {
                 MessageBox.Show("Verifique la hora de inicio y la hora de salida \n Como minimo programe una hora");
                 dateTimePicker1.Focus();
             }
             else if (cbNombrePonente.Text.Trim()==String.Empty)
             {
                 errorProvider1.SetError(cbNombrePonente,"Seleccione docente");
             }
             else
             {
                 var id_docente = cbNombrePonente.SelectedValue.ToString();
                 int id_doc = int.Parse(id_docente);
                 var cup = cbAula.SelectedValue.ToString();
                 int cupos = int.Parse(cup);
                 n.nombre_aula = cbAula.Text.Trim();
                 int id_aula = n.obtener_idAula();
                 var id_sem = cbSeminario.SelectedValue.ToString();
                 int id_seminario = int.Parse(id_sem);
                 int minf = min_f + 1;
                 int mini = min_i + 1;
                 int horaf=hora_f;
                 int horai=hora_i;
                 if (mini==60)
                 {
                     horai = hora_i + 1;
                     mini = 00;
                 }
                 if (minf==60)
                 {
                     horaf = hora_f + 1;
                     minf = 00;
                 }
                 n.hora_f = horaf + ":" + minf;
                 n.hora_i = horai + ":" + mini;
                 n.fecha = año + "-" + mes + "-" + dia;
                 n.id_aula = id_aula;
              
                 n.id_docente = id_doc;
                 var comprobar = n.vericar_seminario();
                 int tiempo = int.Parse(comprobar.Rows[0]["TIEMPO"].ToString());
                 int presonal = int.Parse(comprobar.Rows[0]["PERSONA"].ToString());
                 if (tiempo==1)
                 {
                     MessageBox.Show("Este Horario no esta disponible \n verifique fecha o la hora");
                     cbAula.Focus();
                 }
                 else if (presonal==1)
                 {
                     MessageBox.Show("Este Docente no esta disponible en este horario");
                     cbNombrePonente.Focus();
                 }
                 else
                 {
                     

                     n.id_seminario = id_seminario;
                     n.id_facultad = this.id_facultad == -1 ? int.Parse(cbxFacultad.SelectedValue.ToString()) : this.id_facultad;
                     n.fecha = año + "-" + mes + "-" + dia;
                     n.hora_f = hora_f + ":" + min_f;
                     n.hora_i = hora_i + ":" + min_i;
                     n.id_aula = id_aula;
                     n.cupos = cupos;
                     n.id_docente = id_doc;

                     bool res = n.programar_seminario();
                     if (res)
                     {
                         MessageBox.Show("Seminario Programado");
                         dateTimePicker1.Text = "00:00";
                         dateTimePicker1.Text = "00:00";
                         cbEdificio.SelectedIndex = 0;
                         cbSeminario.SelectedIndex = 0;
                         cbSeminario.SelectedIndex = -1;
                     }
                     else
                     {
                         MessageBox.Show("Algo Salio mal");
                     }
                 }
             }
           
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void cbEdificio_SelectedIndexChanged(object sender, EventArgs e)
        {

            var idEdif = cbEdificio.SelectedValue.ToString();
            if (idEdif != "System.Data.DataRowView")
            {
                int id = int.Parse(idEdif);
                n.id_edificio = id;
                cbAula.DataSource = n.listarAulas();
                cbAula.DisplayMember = "NOM_AULA";
                cbAula.ValueMember = "CAPACIDAD";
            }
        }

        private void cbxFacultad_TextChanged(object sender, EventArgs e)
        {
            var idFac = cbxFacultad?.SelectedValue?.ToString() != null ? cbxFacultad?.SelectedValue?.ToString() : "-1";

            if (idFac != "System.Data.DataRowView")
            {
                int idfacultad = int.Parse(idFac);
                n.id_facultad = idfacultad;
            }
        }

        private void frmProgramarSeminario_Shown(object sender, EventArgs e)
        {
            cbEdificio.SelectedIndex = 3;
            cbEdificio.SelectedIndex = 0;
        }

        private void cbAula_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var capa = cbAula.SelectedValue.ToString();
                if (capa != "System.Data.DataRowView")
                {                
                 int cap = int.Parse(capa);
                 txtCapacidad.Text = cap.ToString();;  
                }
            }
            catch (Exception)
            {                 
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            DateTime fecha =Convert.ToDateTime(dateTimePicker2.Text.Trim().ToString());
            int hora =fecha.Hour;
            int min=fecha.Minute;          
            if (hora > 18 && min > 0 || hora > 18 && min == 0 || hora == 18 && min > 0)
            {
                dateTimePicker2.Text = "18:00";
            }
            else if (hora < 9)
            {
                dateTimePicker2.Text = "09:00";
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(dateTimePicker1.Text.Trim().ToString());
            int hora = fecha.Hour;
            int min = fecha.Minute;
            if (hora<8)
            {
                dateTimePicker1.Text = "08:00";
            }
            else if (hora > 17 && min > 0 || hora > 17 && min == 0 || hora == 17 && min > 0)
            {
                dateTimePicker1.Text = "17:00";
            }
        }

        private void cbxFacultad_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbSeminario.DataSource = n.ConsultarSeminarios();
            cbSeminario.DisplayMember = "nombre";
            cbSeminario.ValueMember = "ID_SEMINARIO";
            cbSeminario.SelectedIndex = -1;
        }

        private void cbNombrePonente_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbNombrePonente,String.Empty);
        }

       

        
    }
}
