﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
namespace Conexion
{
    public class CConexion
    {
       SqlConnection cn = new SqlConnection("Data Source=sql-server-seminarios.database.windows.net;Initial Catalog=PROYECTO_PROG1;User Id=islam;Password='Nscsp a7217';");
        
       
        
        private void AbrirCn()
        {
            
            try
            {
                cn.Open();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CerraCn()
        {
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }
        }
        public bool Consulta(string consulta)
        {
            SqlCommand com = new SqlCommand(consulta, cn);

            try
            {
                AbrirCn();
                com.ExecuteNonQuery();
                CerraCn();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                CerraCn();
            }
        }
        public DataTable consultalvl2(string consulta)
        {
            DataTable tabla = new DataTable();
            try
            {
                SqlDataAdapter adaptador = new SqlDataAdapter(consulta, cn);
                adaptador.SelectCommand.CommandType = CommandType.Text;
                adaptador.Fill(tabla);
                return tabla;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return tabla;
            }
        }
        
    }
}
