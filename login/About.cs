﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            string target = "https://gitlab.com/julioplatero98";
            System.Diagnostics.Process.Start(target);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            string target = "https://gitlab.com/jhosepchai";
            System.Diagnostics.Process.Start(target);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            string target = "https://gitlab.com/seminario4/seminario-desktop-v1";
            System.Diagnostics.Process.Start(target);
        }

        private void label4_Click(object sender, EventArgs e)
        {
            string target = "https://gitlab.com/JoshuaKnight98";
            System.Diagnostics.Process.Start(target);
        }
    }
}
