﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
using Botones;
namespace login
{
    //429EBQW-D2A-90FAC492DN 
    public partial class frmLogin : Form
    {
        Negocios n = new Negocios();
        public frmLogin()
        {
            InitializeComponent();
        }
        private void entrar()
        {
             if (String.IsNullOrEmpty(txtUsuario.Text.Trim()))
            {
                errorProvider1.SetError(txtUsuario, "Campo Obligatorio");
                txtUsuario.Focus();
            }
            else if (String.IsNullOrEmpty(txtContraseña.Text.Trim()))
            {
                errorProvider1.SetError(txtContraseña, "Campo Obligatorio");
                txtContraseña.Focus();
            }
            else
            {
                string Usuario = txtUsuario.Text.Trim();

                string Contraseña = txtContraseña.Text.Trim();
                n.nombre_usuario = Usuario;
                n.pwd = Contraseña;
                var tabla = n.inicioSesion();
                int tamaño = tabla.Rows.Count;
                if (tamaño == 1)
                {
                    string us_obt = tabla.Rows[0]["NOMBRE_USUARIO"].ToString();
                    
                    int lvl = 0;
                    string pwd = null;
                    pwd = tabla.Rows[0]["PWD"].ToString();
                    lvl = int.Parse(tabla.Rows[0]["ID_PRIVILEGIO"].ToString());
                    if (pwd != Contraseña)
                    {
                        MessageBox.Show("Usuario o contraseña Invalidos");
                        txtContraseña.Text = String.Empty;
                    }
                    else if (Usuario!=us_obt)
                    {
                        MessageBox.Show("Usuario o contraseña Invalidos");
                        txtContraseña.Text = String.Empty;
                    }
                    else
                    {
                        n.nombre_usuario = txtUsuario.Text.Trim();                       
                        n.marcar();
                        
                        switch (lvl)
                        {
                            case 1:
                                DialogResult res1;
                                Administrador ad = new Administrador();
                                this.Visible = false;
                                res1 = ad.ShowDialog();
                                if (res1 == DialogResult.Cancel)
                                {
                                    this.Visible = true;
                                    ad.Dispose();
                                    txtContraseña.Text = String.Empty;
                                    txtUsuario.Text = String.Empty;
                                    txtUsuario.Focus();
                                }
                                break;
                            case 2:
                                DialogResult res2;                                
                                Docente doc = new Docente();
                                doc.id_facultad = n.idfacultad();
                                this.Visible = false;
                                res2 = doc.ShowDialog();
                                if (res2 == DialogResult.Cancel)
                                {
                                    this.Visible = true;
                                    doc.Dispose();
                                    txtContraseña.Text = String.Empty;
                                    txtUsuario.Text = String.Empty;
                                    txtUsuario.Focus();
                                }
                                break;
                            case 3:
                                DialogResult res3;
                                Estudiante es = new Estudiante();
                                es.USUARIO= txtUsuario.Text.Trim();                             
                                this.Visible = false;
                                res3 = es.ShowDialog();
                                if (res3 == DialogResult.Cancel)
                                {
                                    this.Visible = true;
                                    es.Dispose();
                                    txtContraseña.Text = String.Empty;
                                    txtUsuario.Text = String.Empty;
                                    txtUsuario.Focus();
                                }
                                break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Usuario o contraseña invalidos");
                    txtContraseña.Text = String.Empty;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            entrar();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtUsuario, String.Empty);
        }
        private void txtContraseña_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtContraseña, String.Empty);
        }
        private void button3_MouseDown(object sender, MouseEventArgs e)
        {
            txtContraseña.UseSystemPasswordChar = false;
        }
        private void button3_MouseUp(object sender, MouseEventArgs e)
        {
            txtContraseña.UseSystemPasswordChar = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(btnVer, "Ver Contraseña");
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter && txtContraseña.Text.Trim()==String.Empty)
            {
                txtContraseña.Focus();
            }
            else if ((int)e.KeyChar == (int)Keys.Enter && txtContraseña.Text.Trim()!=String.Empty)
            {
                entrar();
            }
        }

        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter )
            {
                entrar();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            About about = new About();
            var result = about.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                this.Visible = true;
                about.Dispose();

            }
        }
    }
}
