﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocios;
namespace AgregarSeminario
{
    public partial class frmAgregarSeminario : Form
    {
        public frmAgregarSeminario()
        {
            InitializeComponent();
        }
        Negocios n = new Negocios(); 
        private void AgregarSeminario_Load(object sender, EventArgs e)
		{
            cbxFacultad.DataSource = n.listarFacultades();
            cbxFacultad.DisplayMember = "NOM_FACULTAD";
            cbxFacultad.ValueMember = "ID_FACULTAD";
            cbxFacultad.DropDownStyle = ComboBoxStyle.DropDownList;
		}

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            epError.SetError(txtNombre,String.Empty);
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            epError.SetError(txtCodigo, String.Empty);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            double valor = 0.00;
            if (txtNombre.Text.Trim() == String.Empty)
            {
                epError.SetError(txtNombre, "Campo Requerido");
                txtNombre.Focus();
            }
            else if (txtCodigo.Text.Trim().Length<4)
            {
                epError.SetError(txtCodigo, "formato invalido ingrese este formato ejempo : F-02");
                txtCodigo.Focus();
            }
            else if (txtValor.Text.Trim() == String.Empty)
            {
                epError.SetError(txtValor, "Campo Requerido");
                txtValor.Focus();
            }

            else
            {
                n.cod_Seminario = txtCodigo.Text.Trim();
                int res =n.verificar_existenciaSeminario();
                
                if (res==1)
                {
                    epError.SetError(txtCodigo,"Este codigo ya fue ocupado");
                }
                else
                {
                    
                    var id = cbxFacultad.SelectedValue.ToString();            
                    int idfacu = int.Parse(id);
                    n.nombreSeminario = txtNombre.Text.Trim();
                    n.cod_Seminario = txtCodigo.Text.Trim();
                    n.id_facultad = idfacu;
                    n.valo_seminario = float.Parse(txtValor.Text.Trim().ToString()).ToString();
                    bool men = n.agregarSeminario();
                    if (men==true)
                    {
                        MessageBox.Show("Guardado Existosamente");
                        txtCodigo.Text = String.Empty;
                        txtNombre.Text = String.Empty;
                        cbxFacultad.SelectedIndex = 0;
                        txtValor.Text = String.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Algo Falló");
                    }
                   
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void txtCodigo_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            epError.SetError(txtCodigo,String.Empty);
        }

        private void txtCodigo_TextChanged_1(object sender, EventArgs e)
        {
            epError.SetError(txtCodigo, String.Empty);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
